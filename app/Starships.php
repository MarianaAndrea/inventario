<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Starships extends Model
{
    protected $fillable = [
        // 'id','name', 'model', 'manufacturer', 'cost_in_credits', 'length', 'max_atmosphering_speed','crew','passengers','cargo_capacity', 'consumables', 'hyperdrive_rating','MGLT', 'starship_class', 'pilots', 'films', 'created', 'edited', 'url'
        'id','name', 'model', 'manufacturer'
    ];


    public $timestamps = false;

}
