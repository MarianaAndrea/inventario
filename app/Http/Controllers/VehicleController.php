<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Vehicles;

class VehicleController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct()
    {
    }

    public function listPrueba(){
        $vehicles = Vehicles::all();
        foreach($vehicles as $vehicle){
            echo $vehicle;
        }

        die();
        // return response()->json(['res' => $vehicles, 'httpStateCode' => 201, 'message' => 'Prueba Listado!'], 201);
        // $results = DB::select('select * from vehicles where id = ?', [1]);
    }

}
