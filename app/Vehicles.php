<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicles extends Model
{
   
    protected $fillable = [
        'id','name', 'model', 'manufacturer', 'cost_in_credits', 'length', 'max_atmosphering_speed', 'crew','passengers','cargo_capacity', 'consumables', 'vehicle_class', 'pilots', 'films', 'created', 'edited', 'url'
    ];

    public $timestamps = false;

    
}
