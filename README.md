## Inventario

## Levantando entorno

ejecutar composer install, desde la consola, situados en la raiz del proyecto
ejecutar migrate
ejecutar php artisan serve


## Datos del .env

`
APP_NAME=Inventario
APP_ENV=local
APP_KEY=base64:+T3ySIIPcW/G4UE9JnTEfXkjkPQnNFEc83BxD4NSvsc=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=8080
DB_DATABASE=inventario-sw
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
`